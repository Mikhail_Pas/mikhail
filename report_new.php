<?php
require_once 'param.php';

$sait = 'MestoID' ;
$date = date("Y.m.d H:i:s");

// Конектимся к базе
$db_connected=$db_connected2=false;
$db_connected = db_connect($ar);
$db_connected2 = db_connect2($ar);
if ($db_connected === false) {
    error_log2($ar, 'Sql request error.');
    return;
}
// Файл отчета на сайте находится в одном месте. У меня он находился здесь
// $file = fopen("https://mesto.ua/account/import/report/?access_code=db5ddf2469a74c338b4c76ea65ead3aa", "r");
 $file = fopen("https://mesto.ua/account/import/report/?access_code=4201e756068a4157a2ec63357899c124", "r");
if (!$file) {
    echo "<p>Невозможно открыть удаленный файл.\n";
    exit;
}
$file_csv = fopen("mesto_answer.csv", "a");

// $file_csv = fopen("/var/www/website_export/www/rieltor/csv_files/mesto_answer.csv", "a");
if (!$file_csv) {
    echo "<p>Невозможно открыть удаленный файл.\n";
    exit;
}

$i = 0;
// Файл читаем по строчно и выбираем нужную нам информацию

while (!feof($file)) {
    $line = fgets($file, 1024);
    $line = htmlspecialchars($line) ;

    if (strpos($line,"property")) {

$i = $i + 1 ;

        $id_url = '';
        while ($line <> "&lt;/property&gt;\n") {
            $line = fgets($file, 1024);
            $line = htmlspecialchars($line) ;
            if (stristr($line, "&lt;xml_id&gt;")) {
                $end = strrpos($line,'&lt;');
                $auto = substr($line,18,$end-18);

            }
            if (stristr($line, "errors")) {

                $url_1 = str_replace("&lt;", "<", $line);
                $url_2 = str_replace("&gt;", ">", $url_1);
                $url_3 = str_replace("&quot;", '"', $url_2);
                $url = $url_3;
                $end = strrpos($url,'}');
                $url = substr($url,8,$end -7);
            }
            if (stristr($line, "&lt;url&gt;")) {
                $end = strrpos($line,'&lt;');
                $url = substr($line,19,$end-19);
                $end_1 = strrpos($url,'/');
                $end_2 = strrpos($url,'.html');
                $id_url = substr($url,$end_1 + 1,$end_2 - $end_1 - 1);

            }

        }

        $status = $url;
       echo "$auto $url $id_url \n";
//       echo "$auto $url $id_url <br>";

        if (stristr($url, "{") === FALSE ) {
            $auto_s = null;
            $status = "Опубликовано!";


                $sql_str = "SELECT * FROM `mesto_export` WHERE `AUTO`= '$auto'";
                $sql_a = $ar['db']->query($sql_str);

           $result = $sql_a->fetch_assoc() ;
                    $orderCode = $result["Наш ID"];
                    $publishDate = $result["d_update"];
                    $account_login = $result["Сайт логин"];
                    $phone = $result["Номер телефона для Сайта"];
                    $sql_str = "INSERT INTO  `morning_report` (`sourceId`,
 `reportDate`, `orderCode`, `sourceOrderCode`, `url`, `publishDate`, `accountLogin`, `publishPhone`, `dt_update`) VALUES
('$sait','$date','$orderCode','$id_url','$url','$publishDate', '$account_login', '$phone','$publishDate') ";
                    $sql_m = $ar['db']->query($sql_str);
                    $sql_str = "UPDATE  `adv_export_site_stat` SET  `adv_site_id` = '$id_url', `export_date` = '$date' , `status_msg` = '$status'
 WHERE `export_id`= '$auto' AND `site_id`= '$sait'";

                    $sql_m = $ar['db']->query($sql_str);



        }

        else {

            $sql_str = "SELECT * FROM `adv_export_site_stat` WHERE `export_id`= '$auto' AND `site_id`= '$sait'";
            $sql_s = $ar['db']->query($sql_str);
            while ($result = $sql_s->fetch_assoc()) $auto_s = $result["export_id"];
            $sql_str = "UPDATE  `adv_export_site_stat` SET `status_msg` = '$url' , `adv_site_id` = '$id_url' , `export_date` = '$date'
 WHERE `export_id`= '$auto' AND `site_id`= '$sait'";

            $sql_m = $ar['db']->query($sql_str);
        }

        $sql_str = "SELECT * FROM `mesto_export` WHERE `AUTO`= '$auto'";
        $sql_a = $ar['db']->query($sql_str);

        $result = $sql_a->fetch_assoc() ;
        $orderCode = $result["Наш ID"];

        $publishDate = $result["d_update"];
        $account_login = $result["Сайт логин"];
        $phone = $result["Номер телефона для Сайта"];

        $somecontent = $auto.";".$orderCode.";".$date.";".$id_url.";".$phone.";".$status."\n";
        $somecontent = mb_convert_encoding($somecontent, "cp-1251");
        fwrite($file_csv, $somecontent);
    }

}
fclose($file_csv);
fclose($file);

function db_connect(&$ar)
{
    $mysqli = new mysqli($ar['pdb_host'], $ar['pdb_user'], $ar['pdb_password'], $ar['pdb_basename']);
    if (mysqli_connect_errno()) {

        echo 'Невозможно установить соединение с базой данных ' . $ar['pdb_host'];

        return false;
    } else {

        $mysqli->query("set character_set_client='utf8'");
        $mysqli->query("set character_set_results='utf8'");
        $mysqli->query("set collation_connection='cp1251_general_ci'");
        $ar['db'] = $mysqli;
        return true;
    }
}
function db_connect2(&$ar)
{
    $mysqli = new mysqli($ar['db_host'], $ar['db_user'], $ar['db_password'], $ar['db_basename']);
    if (mysqli_connect_errno()) {
        echo 'Невозможно установить соединение с базой данных ' . $ar['db_host'];

        return false;
    } else {

        $mysqli->query("set character_set_client='utf8'");
        $mysqli->query("set character_set_results='utf8'");
        $mysqli->query("set collation_connection='cp1251_general_ci'");
        $ar['db2'] = $mysqli;
        return true;
    }
}

?>
